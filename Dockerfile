FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/code
WORKDIR /app/code

ARG VERSION=1.1.0

RUN curl -sSf https://install.surrealdb.com | sh -s -- --version v${VERSION}

EXPOSE 8000

ADD start.sh /app/code/start.sh

CMD [ "/app/code/start.sh" ]